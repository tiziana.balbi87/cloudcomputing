import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
  return 'Hello, World!'

if __name__ == "__main__":
  port = int(os.environ.get("PORT", 5000))
  host = os.environ.get("HOST", "0.0.0.0")
  app.run(host = host, port = port)
